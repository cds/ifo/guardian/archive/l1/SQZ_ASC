
##################################################
# SQZ PARAMETERS FILE
##################################################


#The 'no squeezing' flag: 
nosqz = False
'''
Set above to True if you want to set the IFO to Observe 
without the squeezer, you'll need to reload all SQZ guardians
for this change to take effect.
'''
