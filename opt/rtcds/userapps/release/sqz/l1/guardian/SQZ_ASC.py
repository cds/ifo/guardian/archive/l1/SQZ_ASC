# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_LO.py $

#import sys
import time
from guardian import GuardState, GuardStateDecorator
from guardian.state import (
    TimerManager,
)
from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

# this is to keep emacs python-mode happy from lint errors due
# to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    notify = None
    log = None

if sqzconfig.nosqz == True:
    nominal = 'IDLE'
else:
    nominal = 'IDLE'

class SQZASCParams(EzcaUser):
    FAULT_TIMEOUT_S = 1
    
    OMC3_HTH = 3000 # high threshold for OMC3
    OMC3_LTH = 300 # low threshold for OMC3

    CLK_GAIN = {'PIT1':50,
                'PIT2':50,
                'YAW1':100,
                'YAW2':100}
    

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0


    def update_calculations(self):
        """
        No calculations, but keeping forwards compatible if they are wanted
        """
        return

    def check_filter_on(self,chan):
        is_input_on = bool(int(ezca['%s_SW1R'%chan]) & 0b100)
        is_output_on = bool(int(ezca['%s_SW2R'%chan]) & 0b10000000000)
        is_gain_one = not ezca['%s_GAIN'%chan] == 0
        return is_input_on & is_output_on & is_gain_one
        

    def fault_checker(self):
        fault_condition = False

        # OMC RF3 needs to be good value
        if not self.OMC3_HTH > ezca['SQZ-OMC_TRANS_RF3_ABS_OUTPUT'] > self.OMC3_LTH:
            notify('OMC RF3 ABS is out of range. Please align manually first')
            fault_condition = True

        for sus in ['ZM4','ZM5','ZM6']:
            for dof in ['P','Y']:
                if not self.check_filter_on('SUS-%s_M1_LOCK_%s'%(sus,dof)):
                    notify('%s ISC filter is off'%sus)
                    fault_condition = True
                

        if fault_condition:
            if self.timer['FAULT_TIMEOUT']:
                return 
            else:
                return True
        else:
            #reset the timeout every cycle
            self.timer['FAULT_TIMEOUT'] = self.FAULT_TIMEOUT_S
        return False

class SQZASCShared(SharedState):
    @DecoShared
    def sqzasc(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return SQZASCParams()

shared = SQZASCShared()
DOF_LIST = ['PIT1','PIT2','YAW1','YAW2']

#############################################
#Functions


#############################################
#Decorator

# There should be at least two types of fault_checker here.
# One you go back to trying again (lock loss), another you just go straight to down
# (no laser beam).

class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.sqzasc.fault_checker():
            return 'FAULT'

        else:
            shared.sqzasc.timer['FAULT_TIMEOUT'] = shared.sqzasc.FAULT_TIMEOUT_S


#############################################
#States

class INIT(GuardState):
    request = False

    def main(self):
        return True

    @deco_autocommit
    def run(self):
        return True


#exists only to inform the manager to move into its managed state
class MANAGED(GuardState):
    request = True

    @deco_autocommit
    def main(self):
        return True

    @deco_autocommit
    def run(self):
        return True


class IDLE(GuardState):

    @deco_autocommit
    def run(self):
        return True


DOWN_RESET_TIME = 1
save_asc_gains = {}

def down_state_set():
    ezca['SQZ-ASC_WFS_GAIN'] = 0
    for dof in DOF_LIST:
        ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = 0
        ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 0
        


# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    index   = 1
    request = False
    goto    = True

    def main(self):
        down_state_set()
        self.timer['down_settle'] = DOWN_RESET_TIME
        return

    def run(self):
        return True


class FAULT(GuardState):
    redirect = False
    request = False

    def main(self):
        down_state_set()
        return

    def run(self):
        blocked = shared.sqzasc.fault_checker()
        if blocked is None:
            return
        if not blocked:
            return True


class ENGAGING_ADS(GuardState):
    index = 5
    @fault_checker
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0

    @fault_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            for dof in DOF_LIST:
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = shared.sqzasc.CLK_GAIN[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1
            self.timer['waiting'] = 2
            self.counter += 1
            
        elif self.counter == 1:
            # engage servo
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.3:
                self.counter += 1
                return
            ezca['SQZ-ASC_WFS_GAIN'] += 0.1
            self.timer['waiting'] = 0.2
                
        
        elif self.counter == 2:
            return True

class ENGAGING_ZM5_ADS(GuardState):
    index = 6
    @fault_checker
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0

    @fault_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            for dof in ['PIT1','YAW1']:
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = shared.sqzasc.CLK_GAIN[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1                
            self.timer['waiting'] = 2
            self.counter += 1
            
        elif self.counter == 1:
            # engage servo
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.3:
                self.counter += 1
                return
            ezca['SQZ-ASC_WFS_GAIN'] += 0.1
            self.timer['waiting'] = 0.2
                
        
        elif self.counter == 2:
            return True

class ENGAGING_ZM4_ADS(GuardState):
    index = 7
    @fault_checker
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0

    @fault_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            for dof in ['PIT2','YAW2']:
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = shared.sqzasc.CLK_GAIN[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1
            self.timer['waiting'] = 2
            self.counter += 1
            
        elif self.counter == 1:
            # engage servo
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.3:
                self.counter += 1
                return
            ezca['SQZ-ASC_WFS_GAIN'] += 0.1
            self.timer['waiting'] = 0.2
                
        
        elif self.counter == 2:
            return True


class ADS_ENGAGED(GuardState):
    index = 10
    @fault_checker
    def main(self):
        self.timer['lock_settle'] = 2
        return

    @fault_checker
    def run(self):
        if self.timer['lock_settle']:
            return True



##################################################

edges = [
    ('INIT', 'DOWN'),
    ('DOWN','IDLE'),
    ('IDLE', 'ENGAGING_ADS'),
    ('ENGAGING_ADS', 'ADS_ENGAGED'),
    ('FAULT','IDLE'),
    ('IDLE', 'ENGAGING_ZM4_ADS'),
    ('IDLE', 'ENGAGING_ZM5_ADS'),    
]


lock_states = [
    'ADS_LOCKED',
]

